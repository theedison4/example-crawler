/**
 * Created by edisoni on 31.05.17.
 */

const async = require('async');
const cheerio = require('cheerio');
const request = require('request');
const log4js = require('log4js');


const LIST = 1;
const DETAIL = 2;

let logger = log4js.getLogger("EXAMPLE-CRAWLER");

let concurrency = 4;
let baseUrl = 'https://habrahabr.ru';

logger.info('Started crawler');

let crawlerQueue = async.queue((task, cb) => {
    if (task.url.indexOf(baseUrl) == -1) {
        task.url = baseUrl + task.url;
    }
    let {url, type} = task;
    request(url, (err, res, body) => {
        if (err) {
            logger.error(url);
            logger.error(err);
        } else {
            let $ = cheerio.load(body);
            logger.trace(`Processing page: ${url} type: ${type === LIST ? "LIST" : "DETAILED" }`);
            try {
                switch (type) {
                    case LIST:
                        processingList($, cb, task);
                        break;
                    case DETAIL:
                        processingDetails($, cb, task);
                        break;
                }
            } catch (error) {
                logger.error(error);
            }
        }
    })
}, concurrency);

crawlerQueue.push({url: baseUrl, type: LIST});


let listPages = [];
function pushPageList(url) {
    if (listPages.indexOf(url) > -1) {
        return;
    }

    crawlerQueue.push({
        url,
        type: LIST
    })
}

function processingList($, cb, task) {
    let {url} = task;
    $(".post__title_link").each((_, el) => {
        let href = $(el).attr("href");
        crawlerQueue.push({
            url: href,
            type: DETAIL
        })
    });


    //Pagination
    $("#nav-pagess a").each((_, el) => {
        el = $(el);
        let href = el.attr("href");
        pushPageList(href);
    });

    cb();
}

let store = {};
function processingDetails($, cb, task) {
    let {url} = task;
    let post = {};
    post.title = $(".post__title-text").text();
    post.content = $(".post__body_full .content").text();
    post.author = $(".page-header_wrapper .page-header__info-title").text()
    post.hubs = [];
    $(".post__header .hubs a").each((_, el) => {
        el = $(el);
        let hubUrl = el.attr("href");
        let hubTitle = el.text();
        post.hubs.push({url: hubUrl, title: hubTitle});
    });
    store[url] = post;
    cb();
}


crawlerQueue.drain = () => {
    logger.info("Finished");
    // console.log(JSON.stringify(store, null, 2));
};

